# What is this?

A test runner, inspired by jest, mocha and chai.

# Why another test runner?

I was fed up with trying to get any of the above to run with ESM.

# Goals

Provide a test-runner with api similar to the aforementioned test-runners but with out of the box support for esm and with a touch of functional programming.

# Non-Goals

Be a drop-in substitute for jest, mocha or chai. While this project is inspired by those widely-used test-runners my goal is not to provide _exactly_ the same api.

# What makes it different from mocha and the likes?

Every test is async by default which enables the same spec to be run in node or in the browser or even with a browser interface for nodejs tests. So you can even re-run your tests on the backend without `--inspect-brk` and the likes.

It also does not provide much in the way of `expect.it.to.be.the.same` as I found this way of writing tests - while very expressive - not very useful. As soon as you have more complicated datastructures it falls short anyway.  
Instead it's possible to combine assertions and thus build your own custom assert functions rather easily.

# Example

```typescript
// test/some.spec.ts
import {describe, it, assert, either, and, fail, success} from "capuccino";

describe("feature x", () => {
  // basic usage
  it("should work", async () => {
    return assert(true, "expected this to work");
  })

  // handling exceptions
  it("should prove the collatz conjecture", async () => {
    try {
      prove3nPlus1();
      return success;
    } catch(e) {
      return fail(e.message);
    }
  })

  // combine assertions
  it("should either provide a single number or a tuple with a string and a number", async (done) => {
    const result = someFunction();
    const tuple = !Array.isArray(result) || result.length !== 2 ? undefined : result;
    either(
      assert(typeof result === "number", "expected a number"),
      and(
        assert(tuple != null, "expected a tuple")
        assert(typeof tuple[0] === "string", "expected first element of tuple to be a string"),
        assert(typeof tuple[1] === "number", "expected second element of tuple to be a number")
      )
    )
  })
})
```

```shell
npx capuccino test/**.spec.js
```

or just start the whole thing as a server. Doesn't support hot reloading because it just re-bundles all globbed files + dependencies on reload. Should be fast enough, thanks to esbuild.

```shell
npx capuccino --browser test/**.spec.js
```

and if your code depends on other code that's outside of the current directory (i.e. monorepo) and you would like to debug that code as well you can set the working directory like

```shell
npx capuccino --working-dir ../.. test/**.spec.ts
```

# Typescript

Sadly I had to move away from typescript for now as I could not figure out how to integrate `ts-node` without additional setup from your side.
This means that you should also build your test files, ideally with the same config.

I am still working on rectifying this.

## Sourcemaps

If you want sourcemaps I recommend setting `--inlineSourceMap` and `--inlineSources` in some sort of "dev-script". Yes, it greatly increases the size of the generated output, but any bundler should drop those anyway if configured to do so. Depending on whether you want to deliver the transpiled code you could even make this permanent by writing it to your `tsconfig.json`.
