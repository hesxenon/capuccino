import * as Node from "./Node";
import * as Server from "./Server";
import * as Suite from "./Suite";
import { Function, TaskEither } from "@hesxenon/fp-ts";
import { program } from "./Program";

program
  .action((glob, options) => {
    if (options.browser) {
      Server.start(glob, options);
    } else {
      Function.pipe(
        Node.taskAll(glob),
        TaskEither.match(
          (error) => {
            console.error(error);
            process.exit(1);
          },
          (result) => {
            console.log(Suite.Result.format(result));
            process.exit(
              (() => {
                switch (Suite.Result.isOk(result)) {
                  case undefined:
                  case true:
                    return 0;
                  case false:
                    return 1;
                }
              })()
            );
          }
        ),
        (task) => task()
      );
    }
  })
  .parse();
