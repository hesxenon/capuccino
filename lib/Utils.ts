export type path = [string, ...string[]];

export const cache = <fn extends (...args: any) => any>(fn: fn): fn => {
  const none = Symbol("none");
  let cached = none as unknown;
  let cachedArgs = none as unknown;
  return ((...args) => {
    if (cachedArgs !== args || cached === none) {
      cached = fn(...args);
    }
    return cached;
  }) as fn;
};

export const randomFromSet = <a>(set: a[]) =>
  set[Math.floor(Math.random() * set.length)];

export const pseudoUuid = (() => {
  const alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
  const pickRandom = () => randomFromSet(alphabet);
  return (length = 10) => {
    return Array.from({ length }, pickRandom).join("");
  };
})();
