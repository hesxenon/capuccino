import * as Subscriber from "./AsyncSubscriber";

export type subscribe<a> = (subscriber: Subscriber.t<a>) => {
  unsubscribe: () => void;
};
export type emitter<a> = {
  once: subscribe<a>;
  subscribe: subscribe<a>;
  emit: (value: a) => Promise<void>;
};
export type t<a> = emitter<a>;

export const create = <a>(): emitter<a> => {
  const subscribers = [] as Array<Subscriber.t<a>>;

  const subscribe: subscribe<a> = (subscriber) => {
    subscribers.push(subscriber);
    return {
      unsubscribe: () => subscribers.splice(subscribers.indexOf(subscriber), 1),
    };
  };

  const once: subscribe<a> = (subscriber) => {
    const subscription = subscribe(async (value) => {
      await subscriber(value);
      subscription.unsubscribe();
    });
    return subscription;
  };

  return {
    once,
    subscribe,
    emit: (value) =>
      Promise.all(subscribers.map((subscriber) => subscriber(value))).then(
        () => {}
      ),
  };
};
