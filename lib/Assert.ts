import { Array, Function, Option } from "@hesxenon/fp-ts";

export namespace Result {
  export type result = Option.t<string> & {
    expectation: string;
  };
  export type t = result;

  export const ok = (expectation: string): result => ({
    ...Option.none,
    expectation,
  });

  export const error = (expectation: string, error: string): result => ({
    ...Option.some(error),
    expectation,
  });

  export const isOk = (result: result) => Option.isNone(result);
  export const isError = (result: result) => !isOk(result);

  export const map =
    (fn: (error: string) => string) =>
    (result: result): result =>
      Option.match(
        () => ok(result.expectation),
        (currentError: string) => error(result.expectation, fn(currentError))
      )(result);

  export namespace Serialized {
    export type serialized = {
      description: string;
      error: string | undefined;
    };
    export type t = serialized;

    export const serialize = (result: result): serialized => ({
      description: result.expectation,
      error: Option.getOrElse<string | undefined>(() => undefined)(result),
    });

    export const deserialize = (serialized: serialized): result =>
      serialized.error == null
        ? ok(serialized.description)
        : error(serialized.description, serialized.error);
  }
}

export const assert = (condition: boolean, expectation: string): Result.t => {
  const result: Option.Option<string> = condition
    ? Option.none
    : Option.some(expectation);

  return Object.assign(result, {
    expectation,
  });
};

export const fail = (description: string) => assert(false, description);
export const success = assert(true, "");

export const and = (
  ...results: [Result.t, Result.t, ...Result.t[]]
): Result.t => {
  return results.reduce((acc, result) => {
    return Function.pipe(
      acc,
      Option.match(
        () =>
          Function.pipe(
            result,
            Result.map((error) => `'${error}'`)
          ),
        (acc: string) =>
          Function.pipe(
            result,
            Result.map((error) => `${acc} and '${error}'`)
          )
      )
    );
  }, Result.ok("(and)"));
};

export const either = (
  ...results: [Result.t, Result.t, ...Result.t[]]
): Result.t => {
  return Array.compact(results).length < results.length
    ? Result.ok("(either)")
    : Result.error(
        "(either)",
        results.map(({ expectation }) => `'${expectation}'`).join(" or ")
      );
};
