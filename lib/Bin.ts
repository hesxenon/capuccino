#!/usr/bin/env node

import * as Path from "path";
import * as Url from "url";
import { program } from "./Program.js";
import { spawnSync } from "child_process";

const __filename = Url.fileURLToPath(import.meta.url);
const __dirname = Path.dirname(__filename);

program.parse();

const nodeOpts = program.args.slice(0, -1);
const capuccinoOpts = process.argv
  .filter((arg) => !nodeOpts.includes(arg))
  .slice(2);

const script = Path.resolve(__dirname, "Run.js");

const handle = spawnSync(
  "node",
  [
    "--experimental-specifier-resolution=node",
    "--no-warnings",
    ...nodeOpts,
    script,
    ...capuccinoOpts,
  ],
  {
    stdio: "inherit",
  }
);

process.exit(handle.status ?? 0);
