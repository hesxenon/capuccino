export { assert, and, either, fail, success } from "./Assert";
export { assertEqual } from "./Equals";
export { after, afterEach, before, beforeEach, describe } from "./Suite";
export { it } from "./Test";

import type { Result } from "./Assert";
export type result = Result.result;
