import * as R from "ramda";
import * as React from "react";
import * as Suite_ from "../Suite";
import * as Test_ from "../Test";
import * as Utils_ from "../Utils";
import { Function, Option } from "@hesxenon/fp-ts";

const context = React.createContext([Option.none, () => {}] as [
  Option.t<Suite_.Result.t>,
  React.Dispatch<React.SetStateAction<Option.t<Suite_.Result.t>>>
]);

export const Provider = context.Provider;

const useLens = (path: Utils_.path) =>
  React.useMemo(
    () =>
      R.lensPath<Suite_.Result.t, Suite_.Result.t | Test_.Result.t | undefined>(
        path.length === 1
          ? [] // no lens for root suite
          : ["children", ...R.intersperse("children", path.slice(1))] // path includes root id, so start with children and omit root id
      ),
    path
  );

const useResult = (path: Utils_.path) => {
  const [maybeResult] = React.useContext(context);

  const lens = useLens(path);

  return React.useMemo(
    () =>
      Function.pipe(
        maybeResult,
        Option.chain((result) => {
          return Function.pipe(R.view(lens, result), Option.fromNullable);
        })
      ),
    [lens, maybeResult]
  );
};

export const useSuiteResult = (path: Utils_.path) => {
  const result = useResult(path);
  return React.useMemo(
    () =>
      Function.pipe(
        result,
        Option.chain((result) =>
          Suite_.Result.isResult(result) ? Option.some(result) : Option.none
        )
      ),
    [result]
  );
};

export const useTestResult = (path: Utils_.path) => {
  const result = useResult(path);
  return React.useMemo(
    () =>
      Function.pipe(
        result,
        Option.chain((result) =>
          Suite_.Result.isResult(result) ? Option.none : Option.some(result)
        )
      ),
    [result]
  );
};

export const useSetResult = (value: Test_.t | Suite_.t) => {
  const [, setResult] = React.useContext(context);

  const path = React.useMemo(
    () => (Suite_.isSuite(value) ? Suite_.pathOf(value) : Test_.pathOf(value)),
    [value]
  );

  const lens = useLens(path);

  return React.useCallback(
    (result: Test_.Result.t | Suite_.Result.t) =>
      setResult(
        Option.map((state) => {
          const current = R.view(lens, state);
          return current == null
            ? state
            : R.set(lens, R.mergeDeepRight(current, result), state);
        })
      ),
    [lens]
  );
};
