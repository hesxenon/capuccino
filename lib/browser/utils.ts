import * as React from "react";
import { Option, Task } from "@hesxenon/fp-ts";

type useTaskResult<a> = { run: () => void; value: Option.t<a> };
export const useTask = <a>(task: Task.t<a>): useTaskResult<a> => {
  const [value, setValue] = React.useState(Option.none as Option.t<a>);

  const run = React.useCallback(() => {
    setValue(Option.none);
    task().then((value) => setValue(() => Option.some(value)));
  }, [task]);

  return { value, run };
};
