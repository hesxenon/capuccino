import * as Context from "./Context";
import * as React from "react";
import * as Suite_ from "../Suite";
import * as Theme from "./Theme";
import { Boolean, Function, Identity, Option, Task } from "@hesxenon/fp-ts";
import { Test } from "./Test";
import { useTask } from "./utils";

namespace Styles {
  export const container: React.CSSProperties = {
    border: `1px solid ${Theme.Colors.border}`,
    borderRadius: Theme.Radii.md,
    marginBottom: Theme.Margins.md,
  };

  export const header = (success: Option.t<boolean>): React.CSSProperties => ({
    background: Function.pipe(
      success,
      Option.match(
        () => Theme.Colors.backgroundAccent,
        Boolean.match(
          () => Theme.Colors.failure,
          () => Theme.Colors.success
        )
      )
    ),
    padding: Theme.Paddings.md,
    color: Theme.Colors.textLight,
  });

  export const content: React.CSSProperties = {
    padding: Theme.Paddings.md,
  };
}

export const Suite = function Suite({ suite }: Suite.props) {
  const path = React.useMemo(() => Suite_.pathOf(suite), [suite]);
  const result = Context.useSuiteResult(path);
  const setResult = Context.useSetResult(suite);

  const result$ = useTask(
    React.useMemo(
      () =>
        Function.pipe(
          window.env === "web"
            ? Function.pipe(suite, Suite_.task({ includeChildren: false }))
            : () =>
                fetch(
                  `${window.capuccinoBase}/run/suite?path=${path.join(".")}`
                )
                  .then((res) => res.json())
                  .then(Suite_.Result.Serialized.parse),
          Task.map((result) => {
            setResult(result);
            return result;
          })
        ),
      []
    )
  );

  React.useLayoutEffect(() => {
    result$.run();
  }, []);

  const success = React.useMemo(
    () =>
      Function.pipe(
        result,
        Option.chain((result) =>
          Object.keys(result.children).length <
          Object.keys(suite.children).length
            ? Option.none
            : Option.fromNullable(Suite_.Result.isOk(result))
        )
      ),
    [result, suite]
  );

  const children = Object.entries(suite.children).map(([key, value]) =>
    Suite_.isSuite(value) ? (
      <Suite key={key} suite={value} />
    ) : (
      <Test key={key} test={value} />
    )
  );
  return suite.parent == null ? (
    <>{children}</>
  ) : (
    <div style={Styles.container}>
      <div style={Styles.header(success)}>
        [{suite.description}] <button onClick={() => result$.run()}>Run</button>
      </div>
      <div style={Styles.content}>{children}</div>
    </div>
  );
};
export namespace Suite {
  export type props = {
    suite: Suite_.t;
  };
}
