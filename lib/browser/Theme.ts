export namespace Colors {
  export const background = "#f3f3f3";
  export const backgroundContrast = "#3a3a3a";
  export const backgroundAccent = "#22313f";
  export const border = backgroundAccent;
  export const text = "#34495e";
  export const disabled = "#9ba6a5";
  export const textLight = background;
  export const success = "#42b883";
  export const failure = "#d55b3e";
}

export namespace Paddings {
  export const md = "3px";
}

export namespace Margins {
  export const md = "3px";
  export const sm = "1px";
}

export namespace Radii {
  export const md = "3px";
}
