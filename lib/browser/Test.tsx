import * as Context from "./Context";
import * as R from "ramda";
import * as React from "react";
import * as Test_ from "../Test";
import * as Theme from "./Theme";
import { Boolean, Function, Option } from "@hesxenon/fp-ts";
import { useTask } from "./utils";

namespace Styles {
  export const container = (
    success: Option.t<boolean>
  ): React.CSSProperties => {
    const color = Function.pipe(
      success,
      Option.match(
        () => Theme.Colors.disabled,
        Boolean.match(
          () => Theme.Colors.failure,
          () => Theme.Colors.success
        )
      )
    );
    return {
      padding: Theme.Paddings.md,
      color,
      borderLeft: `3px solid ${color}`,
      marginBottom: Theme.Margins.md,
      background: Theme.Colors.background,
    };
  };

  export const button: React.CSSProperties = {
    margin: `0 ${Theme.Margins.md}`,
  };

  export const errorContainer: React.CSSProperties = {
    background: "white",
    padding: Theme.Paddings.md,
    margin: Theme.Margins.md,
    border: "1px solid #dfdfdf",
    borderRadius: Theme.Radii.md,
  };
}

export const Test = function Test({ test }: Test.props) {
  const path = React.useMemo(() => Test_.pathOf(test), [test]);
  const result = Context.useTestResult(path);
  const setResult = Context.useSetResult(test);

  const result$ = useTask(
    React.useCallback(
      () => test.task().then(R.tap((result) => setResult(result))),
      [test.task]
    )
  );

  const success = React.useMemo(
    () =>
      Function.pipe(
        result,
        Option.map(Test_.Result.isOk),
        Option.chain(Option.fromNullable)
      ),
    [result]
  );

  const titleRef = React.useRef<HTMLDivElement>(null);

  const [animation, setAnimation] = React.useState(
    undefined as undefined | Animation
  );

  React.useEffect(() => {
    if (titleRef.current == null) {
      return;
    }

    const animation = titleRef.current.animate(
      [
        {
          opacity: 1,
        },
        {
          opacity: 0,
        },
        {
          opacity: 1,
        },
      ],
      {
        duration: 1000,
        iterations: Infinity,
        delay: 300,
      }
    );
    animation.pause();
    setAnimation(animation);
  }, []);

  React.useEffect(() => {
    if (animation == null) {
      return;
    }
    if (Option.isNone(result$.value)) {
      animation.play();
    } else {
      animation.cancel();
    }
  }, [result$.value]);

  return (
    <div style={Styles.container(success)}>
      <div ref={titleRef}>
        <button style={Styles.button} onClick={result$.run}>
          Run
        </button>
        <span>{test.description}</span>
      </div>
      {Function.pipe(
        result,
        Option.chain((result) => Option.flatten(result.result)),
        Option.map((expectation) => (
          <div style={Styles.errorContainer}>{expectation}</div>
        )),
        Option.getOrElse(() => null as JSX.Element | null)
      )}
    </div>
  );
};
export namespace Test {
  export type props = {
    test: Test_.t;
  };
}
