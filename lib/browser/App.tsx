import * as Context from "./Context";
import * as R from "ramda";
import * as React from "react";
import * as Suite_ from "../Suite";
import * as Test_ from "../Test";
import * as Utils_ from "../Utils";
import { Function, Option, Task } from "@hesxenon/fp-ts";
import { __, match, select } from "ts-pattern";
import { Suite } from "./Suite";
import { useTask } from "./utils";

namespace Node {
  const runTestRemote =
    (path: string[]): Task.t<Test_.Result.t> =>
    () => {
      return fetch(`${window.capuccinoBase}/run/test?path=${path.join(".")}`)
        .then((res) => res.json())
        .then(Test_.Result.Serialized.parse);
    };

  export const fetchSuite = () =>
    // parse suite and "polyfill" task with something that sends a request to the server
    // and waits for the result in the response
    fetch(`${window.capuccinoBase}/suite`)
      .then((res) => res.json())
      .then((serialized: Suite_.Serialized.t): Promise<Suite_.t> => {
        // parse into suite like object
        if (serialized.__tag === "test") {
          return Promise.reject("invalid response, top level must be a suite");
        }

        const parseSuite = (
          serialized: Suite_.Serialized.serializedSuite,
          parent: Suite_.t | undefined
        ) => {
          const suite: Suite_.t = Suite_.create({
            id: serialized.id,
            description: serialized.description,
            parent,
          });
          const mapChildren = R.mapObjIndexed(parseWithParent(suite));
          suite.children = mapChildren(serialized.children);
          return suite;
        };

        const parseWithParent =
          (parent: Suite_.t) =>
          (serialized: Suite_.Serialized.t): Suite_.t | Test_.t => {
            switch (serialized.__tag) {
              case "test": {
                return Test_.of({
                  id: serialized.id,
                  description: serialized.description,
                  task: runTestRemote([
                    ...Suite_.pathOf(parent),
                    serialized.id,
                  ]),
                  suite: parent,
                });
              }
              case "suite": {
                return parseSuite(serialized, parent);
              }
            }
          };

        return Promise.resolve(parseSuite(serialized, undefined));
      });
}

namespace Web {
  export const fetchBundledTests = () =>
    import(`/${window.capuccinoBase}/bundle/tests.js`);
}

export const App = function App() {
  const suite$ = useTask(
    React.useCallback(() => {
      switch (window.env) {
        case "web": {
          // load bundled js
          // Suite.currentSuite represents all tests
          return Web.fetchBundledTests().then(() => Suite_.current);
        }
        case "node": {
          return Node.fetchSuite();
        }
      }
    }, [])
  );

  React.useLayoutEffect(() => {
    suite$.run();
  }, []);

  const resultState = React.useState(Option.none as Option.t<Suite_.Result.t>);

  React.useLayoutEffect(() => {
    const [, setResult] = resultState;
    Function.pipe(suite$.value, Option.map(Suite_.Result.empty), setResult);
  }, [suite$.value]);

  return match({ suite$, result: resultState[0] })
    .with(
      { suite$: { value: Option.some(select()) }, result: Option.some(__) },
      (suite) => (
        <Context.Provider value={resultState}>
          <Suite suite={suite} />
        </Context.Provider>
      )
    )
    .otherwise(() => <div>Loading...</div>);
};
