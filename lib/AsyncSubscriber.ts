export type subscriber<a> = (value: a) => Promise<void>;
export type t<a> = subscriber<a>;

