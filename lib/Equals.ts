import * as Assert from "./Assert";
import { Function, Option } from "@hesxenon/fp-ts";
import { __, match, select } from "ts-pattern";

namespace String {
  export type t = string;

  export const prefix = (with_: string) => (string: string) =>
    `${with_}${string}`;
}

namespace Eq {
  export namespace Result {
    export type result =
      | string
      | { prefix: string; suffix: string; children: result | result[] };
    export type t = result;

    const indent = (level: number) =>
      Array.from({ length: level }, () => "\t").join("");

    export const stringify = (result: result, level = 0): string => {
      return typeof result === "string"
        ? result
        : `${result.prefix}\n${(!Array.isArray(result.children)
            ? [result.children]
            : result.children
          )
            .map(
              (result) => `${indent(level + 1)}${stringify(result, level + 1)}`
            )
            .join("\n")}\n${indent(level)}${result.suffix}`;
    };

    export const aggregate = (
      prefix: (string: string) => string,
      acc: Option.t<result[]>,
      x: Option.t<unknown>,
      y: Option.t<unknown>
    ) => {
      const errors = match({ x, y })
        .with(
          { x: Option.some(select("x")), y: Option.some(select("y")) },
          ({ x, y }) =>
            Function.pipe(
              equals_(x)(y),
              Option.map(
                (result): Eq.Result.t =>
                  typeof result === "string"
                    ? prefix(result)
                    : { ...result, prefix: prefix(result.prefix) }
              )
            )
        )
        .with({ x: Option.some(select()), y: Option.none }, () =>
          Option.some(prefix("is missing"))
        )
        .with({ x: Option.none, y: Option.some(select()) }, () =>
          Option.some(prefix("should not be here"))
        )
        .with({ x: Option.none, y: Option.none }, () => Option.none)
        .exhaustive();

      return match({ acc, errors })
        .with(
          {
            acc: Option.some(select("existing")),
            errors: Option.some(select("errors")),
          },
          ({ existing, errors }) => Option.some([...existing, errors])
        )
        .with({ acc: Option.some(select()) }, (result) => Option.some(result))
        .with({ errors: Option.some(select()) }, (result) =>
          Option.some([result])
        )
        .with({ acc: Option.none, errors: Option.none }, () => Option.none)
        .exhaustive();
    };
  }
  export type t<a> = (x: a) => (u: a) => Option.t<Result.t>;

  export const concat = (xs: string[]) => (us: string[]) => xs.concat(us);
}

namespace Array {
  export const equals: Eq.t<Array<unknown>> = (x) => (u) => {
    return Function.pipe(
      Array.from(
        { length: Math.max(x.length, u.length) },
        (_, index) =>
          [
            index in x ? Option.some(x[index]) : Option.none,
            index in u ? Option.some(u[index]) : Option.none,
          ] as const
      ).reduce((result, [x, u], i) => {
        const prefixIndex = String.prefix(`[${i}]: `);
        return Eq.Result.aggregate(String.prefix(`[${i}]: `), result, x, u);
      }, Option.none as Option.t<Eq.Result.t[]>),
      Option.map((children) => ({
        prefix: "[",
        suffix: "]",
        children,
      }))
    );
  };

  export const of = <a>(...xs: a[]) => xs;

  export const isArray = globalThis.Array.isArray;
  export const from = globalThis.Array.from;
}

namespace Record {
  type k = string | number | symbol;
  export type t<key extends k = string, value = unknown> = Record<key, value>;

  export const isRecord = (x: unknown): x is t =>
    typeof x === "object" && x !== null && !Array.isArray(x);

  const combine = (x: t) => (u: t) => {
    return Array.from(
      new Set([...Object.keys(x), ...Object.keys(u)].sort())
    ).reduce((combined, key) => {
      combined[key] = [
        key in x ? Option.some(x[key]) : Option.none,
        key in u ? Option.some(u[key]) : Option.none,
      ];
      return combined;
    }, {} as Record<k, [Option.t<unknown>, Option.t<unknown>]>);
  };

  export const equals: Eq.t<t> = (x) => (y) => {
    return Function.pipe(
      Object.entries(combine(x)(y)).reduce((acc, [key, [x, y]]) => {
        return Eq.Result.aggregate(String.prefix(`${key}: `), acc, x, y);
      }, Option.none as Option.t<Eq.Result.t[]>),
      Option.map((children) => ({
        prefix: "{",
        suffix: "}",
        children,
      }))
    );
  };
}

/**
 * deeply check if two values are the same and return the differences
 */
const equals_: Eq.t<unknown> = (x) => (u) => {
  if (x === u) {
    return Option.none;
  }
  if (Array.isArray(x) && Array.isArray(u)) {
    return Array.equals(x)(u);
  }
  if (Record.isRecord(x) && Record.isRecord(u)) {
    return Record.equals(x)(u);
  }
  return Option.some(`expected '${u}' to equal '${x}'`);
};

/**
 * assert that two values are deeply equal.
 */
export const assertEqual = (x: unknown, y: unknown): Assert.Result.t => {
  return Function.pipe(
    equals_(x)(y),
    Option.match(
      () => Assert.success,
      (errors) => Assert.fail(Eq.Result.stringify(errors))
    )
  );
};
