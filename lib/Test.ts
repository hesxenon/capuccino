import * as Assert from "./Assert";
import * as Branding from "./Branding";
import * as Subscriber from "./Subscriber";
import * as Suite from "./Suite";
import * as Utils from "./Utils";
import { Function, Option, Task } from "@hesxenon/fp-ts";
import chalk from "chalk";

export namespace Result {
  const _result = Symbol("result");
  export const isResult = Branding.is(_result);
  export type result = Branding.t<
    {
      id: string;
      description: string;
      result: Option.t<Assert.Result.t>;
    },
    typeof _result
  >;
  export type t = result;

  export const of = (result: Branding.unbranded<result>): result =>
    Branding.create(result, _result);

  export const format = ({ description, result }: Result.t) => {
    const checkMark = "\u{2714}";
    const crossMark = "\u{274C}"; // double width char
    return Function.pipe(
      result,
      Option.match(
        () => chalk.gray(description),
        Option.match(
          () => {
            return chalk`{green ${checkMark} ${description}}`;
          },
          (message: string) => {
            return chalk`{red ${crossMark}${description}\n  ${message}}`;
          }
        )
      )
    );
  };

  export const isOk = (result: result) =>
    Function.pipe(
      result.result,
      Option.match(() => undefined, Assert.Result.isOk)
    );

  export const empty = (test: test): result =>
    of({
      id: test.id,
      description: test.description,
      result: Option.none,
    });

  export namespace Serialized {
    export type serialized = {
      id: string;
      description: string;
      result: Assert.Result.Serialized.t | null;
    };
    export type t = serialized;

    export const serialize = (result: result): serialized => ({
      id: result.id,
      description: result.description,
      result: Function.pipe(
        result.result,
        Option.map(Assert.Result.Serialized.serialize),
        Option.toNullable
      ),
    });

    export const parse = (serialized: serialized): result =>
      of({
        id: serialized.id,
        description: serialized.description,
        result: Function.pipe(
          Option.fromNullable(serialized.result),
          Option.map(Assert.Result.Serialized.deserialize)
        ),
      });
  }
}

const _test = Symbol("test");
export type test = Branding.t<
  {
    id: string;
    description: string;
    task: Task.t<Result.t>; // TODO should be a TaskEither, remote runner could fail
    suite: Suite.t;
  },
  typeof _test
>;
export type t = test;

export const of = (test: Branding.unbranded<test>): test =>
  Branding.create(test, _test);

export const it = (
  description: string,
  fn?: ({
    onCleanup,
  }: {
    onCleanup: (subscriber: Subscriber.t<void>) => void;
  }) => Promise<Assert.Result.t>
) => {
  const id = Utils.pseudoUuid();
  const cleanupSubscribers = [] as Array<Subscriber.t<void>>;
  const onCleanup = (subscriber: Subscriber.t<void>) => {
    cleanupSubscribers.push(subscriber);
    return {
      unsubscribe: () =>
        cleanupSubscribers.splice(cleanupSubscribers.indexOf(subscriber), 1),
    };
  };
  const suite = Suite.current;
  const test: test = of({
    id,
    description,
    task: () => {
      return suite.beforeEach.emit().then(() =>
        Promise.race([
          fn == null
            ? Promise.resolve(Option.none)
            : fn({ onCleanup })
                .catch((e) => {
                  return Assert.fail(
                    `${description} threw an error, if you want to fail a test please use \`Assert.fail\`.\nError:\n${e}`
                  );
                })
                .then(Option.some),
          new Promise((resolve) => setTimeout(resolve, 4000)).then(() =>
            Option.some(
              Assert.fail(
                `expected '${description}' to finish within 4 seconds`
              )
            )
          ),
        ])
          .then((result) => Result.of({ id, description, result }))
          .finally(() => {
            cleanupSubscribers.forEach((subscriber) => subscriber());
            return suite.afterEach.emit();
          })
      );
    },
    suite,
  });
  Suite.add(test);
};

export const task = ({ task }: test) => task;
export const skip = async (test: test): Promise<Result.t> =>
  Result.of({
    id: test.id,
    description: test.description,
    result: Option.none,
  });

export const pathOf = (test: test): [string, ...string[]] => [
  ...Suite.pathOf(test.suite),
  test.id,
];
