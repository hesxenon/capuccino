import * as AsyncEmitter from "./AsyncEmitter";
import * as Branding from "./Branding";
import * as R from "ramda";
import * as Test from "./Test";
import * as Utils from "./Utils";
import { Function, Option, Record, Task } from "@hesxenon/fp-ts";
import chalk from "chalk";

const _suite = Symbol("suite");
export const isSuite = Branding.is(_suite);
export type suite = Branding.t<
  {
    id: string;
    description: string;
    children: Record<string, Test.t | suite>;
    parent: suite | undefined;
    before: AsyncEmitter.t<void>;
    beforeEach: AsyncEmitter.t<void>;
    after: AsyncEmitter.t<void>;
    afterEach: AsyncEmitter.t<void>;
  },
  typeof _suite
>;
export type t = suite;

export namespace Result {
  const _result = Symbol("result");
  export const isResult = Branding.is(_result);
  export type result = Branding.t<
    {
      id: string;
      description: string;
      children: Record<string, Test.Result.t | result>;
    },
    typeof _result
  >;
  export type t = result;

  export const of = (result: Branding.unbranded<result>): result =>
    Branding.create(result, _result);

  export const format = (result: result, level = 0): string => {
    const indent = Array.from({ length: level }, () => "  ").join("");
    const strings = [
      ...Object.values(result.children).map((value) =>
        isResult(value)
          ? indent + format(value, level + 1)
          : Test.Result.format(value)
              .split("\n")
              .map((line) => indent + line)
              .join("\n")
      ),
    ];

    if (level > 0) {
      strings.unshift(chalk`{bold [${result.description}]}`);
    }
    return strings.join("\n");
  };

  export const empty = (suite: suite): result => {
    return of({
      id: suite.id,
      description: suite.description,
      children: Function.pipe(
        suite.children,
        Record.map((value) =>
          isSuite(value) ? empty(value) : Test.Result.empty(value)
        )
      ),
    });
  };

  export const isOk = (result: result): boolean | undefined =>
    Object.values(result.children).reduce((acc, result) => {
      if (acc === false) {
        return acc;
      }
      const next = isResult(result) ? isOk(result) : Test.Result.isOk(result);
      return acc == null ? (next === false ? false : acc) : next;
    }, true as undefined | boolean);

  export namespace Serialized {
    export type serialized = {
      id: string;
      description: string;
      children: Record<string, Test.Result.Serialized.t | serialized>;
    };

    export const serialize = (result: result): serialized => ({
      id: result.id,
      description: result.description,
      children: Function.pipe(
        result.children,
        Record.map((result) =>
          isResult(result)
            ? serialize(result)
            : Test.Result.Serialized.serialize(result)
        )
      ),
    });

    export const parse = (serialized: serialized): result =>
      Result.of({
        id: serialized.id,
        description: serialized.description,
        children: Function.pipe(
          serialized.children,
          Record.map((serialized) =>
            "children" in serialized
              ? parse(serialized)
              : Test.Result.Serialized.parse(serialized)
          )
        ),
      });
  }
}

export namespace Serialized {
  type base = { id: string; description: string };
  export type serializedSuite = base & {
    __tag: "suite";
    children: Record<string, serialized>;
  };
  export type serializedTest = base & { __tag: "test" };
  export type serialized = serializedSuite | serializedTest;
  export type t = serialized;

  export const serialize = (suite: suite): serialized => {
    const mapChildren = R.mapObjIndexed(
      (value: Test.t | suite): serialized =>
        isSuite(value)
          ? serialize(value)
          : {
              __tag: "test",
              id: value.id,
              description: value.description,
            }
    );
    return {
      __tag: "suite",
      id: suite.id,
      description: suite.description,
      children: mapChildren(suite.children),
    };
  };
}

export const of = (suite: Branding.unbranded<suite>): suite =>
  Branding.create(suite, _suite);

export const create = (
  base: Omit<
    Branding.unbranded<suite>,
    "before" | "beforeEach" | "after" | "afterEach" | "children"
  >
) => {
  const before = AsyncEmitter.create<void>();
  const beforeEach = AsyncEmitter.create<void>();
  const after = AsyncEmitter.create<void>();
  const afterEach = AsyncEmitter.create<void>();

  return of({
    ...base,
    children: {},
    before,
    beforeEach,
    after,
    afterEach,
  });
};

export let current: suite = create({
  id: Utils.pseudoUuid(),
  description: "",
  parent: undefined,
});

export type context = {
  before: AsyncEmitter.subscribe<void>;
  beforeEach: AsyncEmitter.subscribe<void>;
  after: AsyncEmitter.subscribe<void>;
  afterEach: AsyncEmitter.subscribe<void>;
};

export const describe = (description: string, fn: () => void) => {
  const suite = create({
    id: Utils.pseudoUuid(),
    description,
    parent: current,
  });

  current.children[suite.id] = suite;
  const previous = current;
  current = suite;
  fn();
  current = previous;
};

export const add = (test: Test.t) => {
  if (test.id in current.children) {
    // 1:1E+26 chance per suite... so hopefully never
    throw new Error("id collision detected, please simply re-run the program");
  }
  current.children[test.id] = test;
};

export const before: AsyncEmitter.subscribe<void> = (subscriber) =>
  current.before.subscribe(subscriber);

export const beforeEach: AsyncEmitter.subscribe<void> = (subscriber) =>
  current.beforeEach.subscribe(subscriber);

export const after: AsyncEmitter.subscribe<void> = (subscriber) =>
  current.after.subscribe(subscriber);

export const afterEach: AsyncEmitter.subscribe<void> = (subscriber) =>
  current.afterEach.subscribe(subscriber);

export const task =
  ({ includeChildren }: { includeChildren: boolean }) =>
  (suite: suite): Task.t<Result.t> => {
    const result = Function.pipe(
      Task.sequenceArray(
        Object.values(suite.children).reduce((tasks, value) => {
          if (isSuite(value)) {
            if (includeChildren) {
              tasks.push(task({ includeChildren })(value));
            }
          } else {
            tasks.push(Test.task(value));
          }
          return tasks;
        }, [] as Array<Task.t<Result.t | Test.Result.t>>)
      ),
      Task.map((results) => {
        return Result.of({
          id: suite.id,
          description: suite.description,
          children: results.reduce((record, result) => {
            record[result.id] = result;
            return record;
          }, {} as Result.result["children"]),
        });
      })
    );

    return Function.pipe(
      suite.before.emit,
      Task.chain(() => result),
      Task.chain((result) =>
        Function.pipe(
          suite.after.emit,
          Task.map(() => result)
        )
      )
    );
  };

/**
 * access a child by parts of a path on a given suite.
 * Will disregard excessive keys and return the earliest test on the path
 */
export const access =
  (suite: suite) =>
  ([head, ...tail]: [string, ...string[]]): Option.t<suite | Test.t> => {
    const child = suite.id === head ? suite : suite.children[head];
    return child == null
      ? Option.none
      : tail.length === 0 || !isSuite(child)
      ? Option.some(child)
      : access(child)(tail as [string, ...string[]]);
  };

export const pathOf = (suite: suite): [string, ...string[]] => {
  const parts = [suite.id] as [string, ...string[]];

  (function prepend(suite: suite | undefined) {
    if (suite == null) {
      return;
    }
    parts.unshift(suite.id);
    prepend(suite.parent);
  })(suite.parent);

  return parts;
};
