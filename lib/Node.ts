import * as Path from "path";
import * as Suite from "./Suite";
import { Either, Function, Task, TaskEither } from "@hesxenon/fp-ts";
import chalk from "chalk";
import glob from "glob";

export const matchFiles =
  (pattern: string): TaskEither.t<Error, string[]> =>
  () =>
    new Promise((resolve) =>
      glob(pattern, (error, files) => {
        if (error != null) {
          return resolve(Either.left(error));
        }
        resolve(Either.right(files));
      })
    );

export const importFiles =
  (files: string[]): Task.t<void> =>
  () => {
    return Promise.all(
      files.map((file) => {
        const sizeOfChildren = () => Object.keys(Suite.current.children).length;

        const sizeBeforeImport = sizeOfChildren();
        const path = Path.resolve(process.cwd(), file);
        return import(path)
          .then(() => {
            if (sizeBeforeImport === sizeOfChildren()) {
              console.log(chalk`{gray no tests found in '${file}'!}`);
            }
          })
          .catch((error) =>
            console.log(chalk`{red could not import ${file}}\n`, error)
          );
      })
    ).then(() => {});
  };

export const taskAll = (pattern: string) => {
  return Function.pipe(
    matchFiles(pattern),
    TaskEither.chainTaskK(
      Function.flow(
        importFiles,
        Task.chain(() =>
          Function.pipe(Suite.current, Suite.task({ includeChildren: true }))
        )
      )
    )
  );
};
