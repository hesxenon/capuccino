type record = Record<string | number | symbol, unknown>;

export type t<value extends record, s extends symbol> = value & {
  __tag: s;
};
export type unbranded<a extends t<any, any>> = Omit<a, "__tag">;

export const create = <value extends record, s extends symbol>(
  value: value,
  tag: s
): t<value, s> => ({ ...value, __tag: tag });

export const is =
  <s extends symbol>(tag: s) =>
  (x: unknown): x is t<record, s> =>
    typeof x === "object" &&
    x != null &&
    "__tag" in x &&
    (x as record)["__tag"] === tag;
