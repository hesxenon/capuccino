import { Command } from "commander";
import { readFileSync } from "fs";

const { name, version } = JSON.parse(readFileSync("./package.json", "utf8"));

const invalidArgument = (argument: string, message: string) =>
  `Invalid Argument --${argument}: ${message}`;

export const program = new Command();
program
  .name(name)
  .version(version)
  .allowUnknownOption()
  .argument("<glob>", "the glob pattern to find test files")
  .option(
    "-b, --browser",
    "start a webserver where you can interactively re-run your tests"
  )
  .option("-p, --port <port>", "which port to run the webserver on")
  .option("-n, --node", "run your tests in a node environment")
  .option(
    "--working-dir <working-dir>",
    "set the working directory of the test process"
  )
  .action((_, options) => {
    if (!options.browser) {
      if (options.port) {
        console.log(
          invalidArgument(
            "port",
            "can only be specified in conjunction with --browser"
          )
        );
        process.exit(1);
      }

      if (options.node) {
        console.log(
          invalidArgument(
            "port",
            "can only be specified in conjunction with --browser"
          )
        );
        process.exit(1);
      }
    }
  });
