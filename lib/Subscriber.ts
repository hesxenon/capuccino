export type subscriber<a> = (value: a) => void;
export type t<a> = subscriber<a>;
