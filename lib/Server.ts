import * as Esbuild from "esbuild";
import * as Fs from "fs";
import * as Fsp from "fs/promises";
import * as Http from "http";
import * as Node from "./Node";
import * as Path from "path";
import * as R from "ramda";
import * as Suite from "./Suite";
import * as Test from "./Test";
import * as Url from "url";
import * as Utils from "./Utils";
import { Either, Function, Identity, Task, TaskEither } from "@hesxenon/fp-ts";
import chalk from "chalk";

const capuccinoBase = `capuccino-${Utils.pseudoUuid()}`;

namespace Response {
  export type response = {
    status: number;
    content: string;
    headers: Record<string, string>;
  };
  export type t = response;

  export const text = (content: string, status = 200): response => ({
    status,
    content,
    headers: {},
  });
  export const js = (content: string, status = 200): response => ({
    status,
    content,
    headers: { "Content-Type": "text/javascript" },
  });
  export const bundled = (bundle: Record<string, string>, key: string) => {
    const content = bundle[`bundle/${key}`];
    return content == null
      ? Either.left(Response.text(`could not load bundled file ${key}`, 404))
      : Either.right(Response.js(content));
  };

  export const chunk = (bundle: Record<string, string>) => (key: string) =>
    bundled(bundle, `chunk?id=${key}`);

  export const resolve = (either: Either.Either<response, response>) =>
    Either.getOrElse(R.identity)(either);
}

const ensureParamIsString = Either.chain((param: string | string[]) =>
  Array.isArray(param)
    ? Either.left(
        Response.text("don't know how to handle array in query param")
      )
    : Either.right(param)
);

const ifNone = (msg: string) =>
  Either.chain(Either.fromOption(() => Response.text(msg, 404)));

const toPath = Either.chain((param: string) => {
  const trimmed = param.trim();
  return trimmed.length === 0
    ? Either.left(Response.text("expected non-empty string"))
    : Either.right(trimmed.split(".") as Utils.path);
});

const ensureIsSuite = (msg: string) =>
  Either.chain((value: Test.t | Suite.t) =>
    !Suite.isSuite(value)
      ? Either.left(Response.text(msg, 401))
      : Either.right(value)
  );

const ensureIsTest = (msg: string) =>
  Either.chain((value: Test.t | Suite.t) =>
    Suite.isSuite(value)
      ? Either.left(Response.text(msg, 401))
      : Either.right(value)
  );

export const start = async (
  glob: string,
  options: {
    port?: number;
    node?: boolean;
    workingDir?: string;
  }
) => {
  const port = options.port ?? 8080;

  const cwd = process.cwd();

  const absoluteDir =
    options.workingDir == null ? cwd : Path.resolve(options.workingDir);

  const getRelativeDir = (dir: string) =>
    dir === absoluteDir ? "." : dir.replace(absoluteDir + "/", "");

  const relativeDir = getRelativeDir(cwd);

  const capuccinoBuildDir = Path.dirname(
    Path.dirname(import.meta.url.replace(new RegExp(".*?//"), ""))
  );

  const servedir = Path.normalize(Path.join(capuccinoBuildDir, "../www"));

  const index = Fs.readFileSync(Path.resolve(servedir, "index.html"))
    .toString()
    .replace(/{{ capuccinoBase }}/g, capuccinoBase)
    .replace(/{{ env }}/, options.node ? "node" : "web");

  const bundle = await Function.pipe(
    options.node ? TaskEither.of([]) : Node.matchFiles(glob),
    TaskEither.chainTaskK(
      (files) => () =>
        Esbuild.build({
          absWorkingDir: absoluteDir,
          entryPoints: {
            index: `${capuccinoBuildDir}/lib/browser/index.js`,
            ...Object.fromEntries(
              files.map((file) => [`test/${file}`, `${absoluteDir}/${file}`])
            ),
          },
          format: "esm",
          target: "es2020",
          outdir: "bundle",
          outbase: "build",
          bundle: true,
          splitting: true,
          chunkNames: "chunk?id=[name]-[hash]",
          write: false,
          sourcemap: "inline",
        }).then(({ outputFiles }) =>
          Object.fromEntries(
            outputFiles.map((file) => [
              Path.relative(absoluteDir, file.path),
              file.text,
            ])
          )
        )
    ),
    TaskEither.match((error) => {
      console.error(error);
      process.exit(1);
    }, Identity.of),
    (task) => task()
  );

  const testsJs = Object.entries(bundle)
    .reduce((testFiles, [key, content]) => {
      if (key.startsWith("bundle/test")) {
        testFiles.push(`/////////////// ${key} //////////////\n${content}`);
      }
      return testFiles;
    }, [] as string[])
    .join("\n")
    .replace(
      /import(?:.|\n)*from "(.*)\/chunk\?id/gm,
      (string, relativePath) => {
        return string.replace(relativePath, ".");
      }
    );

  Http.createServer((req, res) => {
    const url = Url.parse(req.url ?? "/", true);

    const queryParam = (key: string) => {
      const param = url.query[key];
      return param == null
        ? Either.left(Response.text(`query param ${param} must be set`, 400))
        : Either.right(param);
    };

    const getPath = () =>
      Function.pipe(queryParam("path"), ensureParamIsString, toPath);

    const data$ = (async (): Promise<Response.t> => {
      switch (url.pathname) {
        case "/":
        case "/index.html": {
          return Response.text(index);
        }
        case `/${capuccinoBase}/bundle/index.js`: {
          return Response.resolve(Response.bundled(bundle, "index.js"));
        }
        case `/${capuccinoBase}/bundle/tests.js`: {
          return Response.js(testsJs);
        }
        case `/${capuccinoBase}/bundle/chunk`: {
          return Function.pipe(
            queryParam("id"),
            ensureParamIsString,
            Either.chain(Response.chunk(bundle)),
            Response.resolve
          );
        }
        case `/${capuccinoBase}/suite`: {
          return Function.pipe(
            Node.matchFiles(glob),
            TaskEither.chainTaskK(Node.importFiles),
            TaskEither.map(() => Suite.current),
            TaskEither.match((error) => {
              console.error(error);
              return Response.text("server error", 500);
            }, Function.flow(Suite.Serialized.serialize, JSON.stringify, Response.text)),
            (task) => task()
          );
        }
        case `/${capuccinoBase}/run/suite`: {
          return Function.pipe(
            getPath(),
            Either.map(Suite.access(Suite.current)),
            ifNone("suite not found"),
            ensureIsSuite("expected path to a suite"),
            Either.match(
              (res) => Promise.resolve(res),
              Function.flow(
                Suite.task({ includeChildren: false }),
                Task.map(
                  Function.flow(
                    Suite.Result.Serialized.serialize,
                    JSON.stringify,
                    Response.js
                  )
                ),
                (task) => task()
              )
            )
          );
        }
        case `/${capuccinoBase}/run/test`: {
          return Function.pipe(
            getPath(),
            Either.map(Suite.access(Suite.current)),
            ifNone("test not found"),
            ensureIsTest("expected path to a test"),
            Either.match(
              (res) => Promise.resolve(res),
              Function.flow(
                Test.task,
                Task.map(
                  Function.flow(
                    Test.Result.Serialized.serialize,
                    JSON.stringify,
                    Response.js
                  )
                ),
                (task) => task()
              )
            )
          );
        }
        default: {
          try {
            const content = await Fsp.readFile(
              `${absoluteDir}${req.url}`,
              "utf8"
            );
            return Response.text(content);
          } catch (e) {
            console.error(e);
            return Response.text("404 - not found", 404);
          }
        }
      }
    })();

    data$
      .then((data) => {
        res.statusCode = data.status;
        Object.entries(data.headers).forEach(([key, value]) =>
          res.setHeader(key, value)
        );
        res.write(data.content);
        res.end();
      })
      .catch((error) => {
        console.error(error);
        res.statusCode = 500;
        res.write("internal server error");
        res.end();
      });
  }).listen(port, () =>
    console.log(chalk`capuccino test-server listening on {magenta :${port}}`)
  );
};
