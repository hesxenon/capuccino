import {
  after,
  afterEach,
  and,
  assert,
  before,
  beforeEach,
  describe,
  either,
  it,
  success,
} from "../lib";

it("should work", async () => {
  return assert(true, "assertion");
});

describe("test suite", () => {
  it("bla", async () => assert(true, "assertion"));
  it("should fail", async () => assert(false, "error"));

  describe("nested", () => {
    it("should also run in a nested describe", async () =>
      assert(true, "should run"));
  });
});

describe("combinators", () => {
  describe("and combinator", () => {
    it("should be possible to combine assertions with and", async () =>
      and(assert(true, ""), assert(true, "")));

    it("should fail if one assertion fails", async () =>
      and(assert(true, ""), assert(false, "this should trigger a total fail")));

    it("should combine error messages", async () =>
      and(assert(false, "a"), assert(false, "b")));

    it("should be possible to combine more than two error messages", async () =>
      and(assert(false, "a"), assert(false, "b"), assert(false, "c")));
  });

  describe("either combinator", () => {
    it("should work", async () => either(assert(true, ""), assert(true, "")));

    it("should not fail if at least one assertion is true", async () =>
      either(assert(true, ""), assert(false, "")));

    it("should combine all result error messages if all assertions fail", async () =>
      either(assert(false, "a"), assert(false, "b")));

    it("should combine more than two error messages", async () =>
      either(assert(false, "a"), assert(false, "b"), assert(false, "c")));
  });
});

describe("api", () => {
  it("should be possible to omit the function");
});

describe("hooks", () => {
  let counter = {
    before: 0,
    beforeEach: 0,
    after: 0,
    afterEach: 0,
  };
  before(async () => {
    counter.before++;
  });

  beforeEach(async () => {
    counter.beforeEach++;
  });

  it("before should run at the beginning of the suite", async () =>
    assert(counter.before === 1, "expected before counter to equal one"));

  it("beforEach should run before each test", async () =>
    assert(
      counter.beforeEach === 2,
      "expected beforeEach counter to equal two"
    ));

  afterEach(async () => {
    counter.afterEach++;
  });

  after(async () => {
    counter.after++;
  });
});
